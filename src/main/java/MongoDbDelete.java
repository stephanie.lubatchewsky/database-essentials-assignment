import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.Document;

public class MongoDbDelete {
    public static void main(String[] args) {
        String uri = "mongodb://localhost:27017";
        MongoClient client = MongoClients.create(uri);

        MongoDatabase database = client.getDatabase("mongo-database");
        MongoCollection<Document> inventory = database.getCollection("inventory");

        //deletar vários registros
        inventory.deleteMany(Filters.eq("nome", "journal"));

        //deletar apenas um registro
        inventory.deleteOne(Filters.gt("qty", 50));
    }
}
