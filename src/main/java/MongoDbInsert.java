import com.mongodb.DBObject;
import com.mongodb.client.*;
import com.mongodb.*;
import org.bson.Document;

import java.util.Arrays;
import java.util.List;

public class MongoDbInsert {
    public static void main(String[] args) {
        //A uri pode ser a de uma base na nuvem também
        String uri = "mongodb://localhost:27017";

        //uri se for passar parametros de conexão
        //String uri = "mongodb://superuser:passw0rd@localhost:27017";

        MongoClient client = MongoClients.create(uri);

        //Equivalente ao show dbs
        MongoIterable<String> databaseNames = client.listDatabaseNames();
        databaseNames.forEach(name -> System.out.println(name));

        //Cria uma nova base e uma nova collection (para aparecer como base criada, precisa ter ao menos uma collection
        MongoDatabase database = client.getDatabase("mongo-database");
        MongoCollection<Document> inventory = database.getCollection("inventory");

        //primeiro jeito de se criar um documento a ser incluído
        Document journal = new Document("name", "journal")
                .append("qty", 25);
        Document score = new Document("score", 9);
        journal.append("rating", List.of(score));
        Document size = new Document("height", 14)
                .append("width", 21)
                .append("unit", "cm");
        journal.put("size", size);
        journal.append("status", "A");
        journal.append("tags", Arrays.asList("brown", "lined"));
        inventory.insertOne(journal);

        //segundo jeito de se criar um documento a ser incluído
        Document notebook = Document.parse("{" +
                "name: 'notebook'," +
                "qty: 50," +
                "size: {" +
                "height: 8.5, width: 11, unit: 'in'" +
                "}," +
                "rating: [{score: 7}, {score: 8}]," +
                "status: 'A'," +
                "tags: ['college - ruled','perforated']" +
                "}");

        Document paper = Document.parse("{" +
                "name: 'paper'," +
                "qty: 100," +
                "size: {" +
                "height: 8.5, width: 11, unit: 'in'" +
                "}," +
                "rating: [{score: 10}]," +
                "status: 'D'," +
                "tags: ['watercolor']" +
                "}");

        Document planner = Document.parse("{" +
                "name: 'planner'," +
                "qty: 75," +
                "size: {" +
                "height: 22.85, width: 30, unit: 'cm'" +
                "}," +
                "rating: [{score: 10}]," +
                "status: 'D'," +
                "tags: ['2019']" +
                "}");

        Document postcard = Document.parse("{" +
                "name: 'postcard'," +
                "qty: 45," +
                "size: {" +
                "height: 10, unit: 'cm'" +
                "}," +
                "rating: [{score: 2}]," +
                "status: 'D'," +
                "tags: ['double-sided', 'white']" +
                "}");

        inventory.insertMany(List.of(notebook, paper, planner, postcard));

    }
}
