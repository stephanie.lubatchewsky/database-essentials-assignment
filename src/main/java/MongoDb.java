import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.Document;

import java.util.Arrays;
import java.util.List;

public class MongoDb {
    public static void main(String[] args) {
        //A uri pode ser a de uma base na nuvem também
        String uri = "mongodb://localhost:27017";

        //uri se for passar parametros de conexão
        //String uri = "mongodb://superuser:passw0rd@localhost:27017";

        MongoClient client = MongoClients.create(uri);

        //Equivalente ao show dbs
        MongoIterable<String> databaseNames = client.listDatabaseNames();
        databaseNames.forEach(name -> System.out.println(name));

        //Cria uma nova base e uma nova collection (para aparecer como base criada, precisa ter ao menos uma collection
        MongoDatabase database = client.getDatabase("mongo-database");
        MongoCollection<Document> inventory = database.getCollection("inventory");

        insert(inventory);
        query(inventory);
        update(inventory);
        delete(inventory);

    }

    public static void insert(MongoCollection<Document> inventory){
        //primeiro jeito de se criar um documento a ser incluído
        Document journal = new Document("name", "journal")
                .append("qty", 25);
        Document score = new Document("score", 9);
        journal.append("rating", List.of(score));
        Document size = new Document("height", 14)
                .append("width", 21)
                .append("unit", "cm");
        journal.put("size", size);
        journal.append("status", "A");
        journal.append("tags", Arrays.asList("brown", "lined"));
        inventory.insertOne(journal);

        //segundo jeito de se criar um documento a ser incluído
        Document notebook = Document.parse("{" +
                "name: 'notebook'," +
                "qty: 50," +
                "size: {" +
                "height: 8.5, width: 11, unit: 'in'" +
                "}," +
                "rating: [{score: 7}, {score: 8}]," +
                "status: 'A'," +
                "tags: ['college - ruled','perforated']" +
                "}");

        Document paper = Document.parse("{" +
                "name: 'paper'," +
                "qty: 100," +
                "size: {" +
                "height: 8.5, width: 11, unit: 'in'" +
                "}," +
                "rating: [{score: 10}]," +
                "status: 'D'," +
                "tags: ['watercolor']" +
                "}");

        Document planner = Document.parse("{" +
                "name: 'planner'," +
                "qty: 75," +
                "size: {" +
                "height: 22.85, width: 30, unit: 'cm'" +
                "}," +
                "rating: [{score: 10}]," +
                "status: 'D'," +
                "tags: ['2019']" +
                "}");

        Document postcard = Document.parse("{" +
                "name: 'postcard'," +
                "qty: 45," +
                "size: {" +
                "height: 10, unit: 'cm'" +
                "}," +
                "rating: [{score: 2}]," +
                "status: 'D'," +
                "tags: ['double-sided', 'white']" +
                "}");

        inventory.insertMany(List.of(notebook, paper, planner, postcard));
    }

    public static void query(MongoCollection<Document> inventory) {
        //encontra todos os documentos da collection
        FindIterable<Document> fullInventory = inventory.find();
        System.out.println("\n**** FULL INVENTORY ****");
        fullInventory.forEach(item -> System.out.println(item.toJson()));

        //encontra todos os documentos com filtro aplicado
        FindIterable<Document> partialInventory = inventory.find(new Document("status", "D"));
        System.out.println("\n**** PARTIAL INVENTORY ****");
        partialInventory.forEach(item -> {
            System.out.println(item.get("name") + " " + item.getString("status"));
        });

        //outra forma de aplicar filtros
        FindIterable<Document> quantity = inventory.find(Filters.lt("qty", 50));
        System.out.println("\n**** QUANTITY < 50 INVENTORY ****");
        quantity.forEach(item -> {
            System.out.println(item.get("name") + " " + item.get("qty"));
        });


        //pesquisando com filtros dentro de outros documentos/objetos
        FindIterable<Document> size = inventory.find(Filters.gt("size.height", 10));
        System.out.println("\n**** SIZE > 10 INVENTORY ****");
        quantity.forEach(item -> {
            System.out.println(item.get("name") + " " + item.get("size"));
        });

        //combinando filtros ao pesquisar
        FindIterable<Document> result = inventory.find(
                Filters.and(Filters.gt("size.height", 10),
                        Filters.eq("status", "D"))
        );
        System.out.println("\n**** COMBINED SEARCH INVENTORY ****");
        result.forEach(item -> {
            System.out.println(item.get("name") + " " + item.get("size") + " " + item.get("status"));
        });
    }

    public static void update(MongoCollection<Document> inventory) {
        //atualizando apenas um registro com apenas um filtro
        inventory.updateOne(Filters.eq("name", "notebook"),
                Updates.set("qty", 200));

        //atualizando vários registros
        inventory.updateMany(Filters.gt("qty", 50),
                Updates.set("status", "C"));

        //atualizando vários registros com mais de um filtro
        inventory.updateMany(Filters.and(Filters.eq("status", "D"), Filters.gt("qty", 100)),
                Updates.set("status", "B"));
    }

    public static void delete(MongoCollection<Document> inventory) {
        inventory.deleteMany(Filters.eq("status", "C"));
    }
}
