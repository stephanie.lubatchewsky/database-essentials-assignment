import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.Document;

public class MongoDbUpdate {
    public static void main(String[] args) {
        String uri = "mongodb://localhost:27017";
        MongoClient client = MongoClients.create(uri);

        MongoDatabase database = client.getDatabase("mongo-database");
        MongoCollection<Document> inventory = database.getCollection("inventory");

        //atualizando apenas um registro com apenas um filtro
        inventory.updateOne(Filters.eq("name", "notebook"),
                Updates.set("qty", 200));

        //atualizando vários registros
        inventory.updateMany(Filters.gt("qty", 50),
                Updates.set("status", "C"));

        //atualizando vários registros com mais de um filtro
        inventory.updateMany(Filters.and(Filters.eq("status", "D"), Filters.gt("qty", 100)),
                Updates.set("status", "B"));


    }
}
