import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import org.bson.Document;

import javax.print.Doc;
import java.util.Arrays;
import java.util.List;

public class MongoDbQuery {
    public static void main(String[] args) {
        String uri = "mongodb://localhost:27017";
        MongoClient client = MongoClients.create(uri);

        MongoDatabase database = client.getDatabase("mongo-database");
        MongoCollection<Document> inventory = database.getCollection("inventory");

        //encontra todos os documentos da collection
        FindIterable<Document> fullInventory = inventory.find();
        System.out.println("\n**** FULL INVENTORY ****");
        fullInventory.forEach(item -> System.out.println(item.toJson()));

        //encontra todos os documentos com filtro aplicado
        FindIterable<Document> partialInventory = inventory.find(new Document("status", "D"));
        System.out.println("\n**** PARTIAL INVENTORY ****");
        partialInventory.forEach(item -> {
            System.out.println(item.get("name") + " " + item.getString("status"));
        });

        //outra forma de aplicar filtros
        FindIterable<Document> quantity = inventory.find(Filters.lt("qty", 50));
        System.out.println("\n**** QUANTITY < 50 INVENTORY ****");
        quantity.forEach(item -> {
            System.out.println(item.get("name") + " " + item.get("qty"));
        });


        //pesquisando com filtros dentro de outros documentos/objetos
        FindIterable<Document> size = inventory.find(Filters.gt("size.height", 10));
        System.out.println("\n**** SIZE > 10 INVENTORY ****");
        quantity.forEach(item -> {
            System.out.println(item.get("name") + " " + item.get("size"));
        });

        //combinando filtros ao pesquisar
        FindIterable<Document> result = inventory.find(
                Filters.and(Filters.gt("size.height", 10),
                        Filters.eq("status", "D"))
        );
        System.out.println("\n**** COMBINED SEARCH INVENTORY ****");
        result.forEach(item -> {
            System.out.println(item.get("name") + " " + item.get("size") + " " + item.get("status"));
        });
    }
}
